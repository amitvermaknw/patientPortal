import { Injectable } from '@angular/core';
import {AbstractControl} from '@angular/forms';
import { ToasterService, ToasterConfig } from 'angular2-toaster/angular2-toaster';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/Rx';

export interface ILoader {
    isLoading: boolean;
}

@Injectable()
export class CommonService {
    
    toasterconfig: ToasterConfig = new ToasterConfig({
        positionClass: 'toast-top-full-width',
        showCloseButton: true,
        timeout: 10000,
        mouseoverTimerStop: false
    });

   

    loader: ILoader = { isLoading: false };

    constructor(private toasterService: ToasterService, private router: Router) {}

    /**
        JWT Authentication
    */
    jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.access_token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.access_token });
            return new RequestOptions({ headers: headers });
        }
    }

    /**
        Toaster lookup
    */
    MsgLookup(toasterType: string, toasterTitle: string, toasterText: string) {
        this.toasterService.pop(toasterType, toasterTitle, toasterText);
    };

    /**
        Add Alert message
    */
    public alerts: Array < any >= [];
    public addAlert(messages: any, altType: any, ) {
        return this.alerts.push({ msg: messages, type: altType });
    }

    /**
    */
    public MatchPassword(AC: AbstractControl) {
       let password = AC.get('Password').value; // to get value in input tag
       let confirmPassword = AC.get('ConfirmPassword').value; // to get value in input tag
        if(password != confirmPassword) {
            AC.get('ConfirmPassword').setErrors( {MatchPassword: true} )
        } else {
            return null
        }
    }

    /**
        Show/Hide Loader
    */
    /*showLoader() {
        console.log('showloader started');
        this.loader.isLoading = true;
    }

    hideLoader() {
        this.loader.isLoading = false;
    }*/

    /**
    Loader class
    */
    showLoader() {
        let el = document.getElementById('main-body');
        let className = 'loading'
        if (el.classList) {
            el.classList.add(className);
        } else if (!this.hasClass(el, className)) {
            el.className += ' ' + className;
        }
    }

    hideLoader() {
        let el = document.getElementById('main-body');
        let className = 'loading'
        if (el.classList) {
            el.classList.remove(className);
        } else if (this.hasClass(el, className)) {
            let reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
            el.className = el.className.replace(reg, ' ');
        }
    }

    hasClass(el, className) {
        if (el.classList) {
            return el.classList.contains(className);
        } else {
            return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
        }
    }


    handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            errMsg = body.error_description || JSON.stringify(body);
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Promise.reject(errMsg);
    }

    mobileView() {
        let headertext = [];
        let headers = document.querySelectorAll("thead");
        let tablebody = document.querySelectorAll("tbody");
    
        for(let i = 0; i < headers.length; i++) { headertext[i]=[]; for (var j = 0, headrow; headrow = headers[i].rows[0].cells[j]; j++) { var current = headrow; headertext[i].push(current.textContent.replace(/\r?\n|\r/,"")); } } if (headers.length > 0) {
            for (let h = 0, tbody; tbody = tablebody[h]; h++) {
                for (let i = 0, row; row = tbody.rows[i]; i++) {
                    for (let j = 0, col; col = row.cells[j]; j++) {
                        col.setAttribute("data-th", headertext[h][j]);
                    } 
                }
            }
        }

    }


}
