import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { SettingsService } from '../../../core/settings/settings.service';
import { CommonService } from '../../../shared/common/common.service';

@Injectable()
export class UserblockService {
    private userBlockVisible: boolean;
    constructor(
                private http: Http,
                private url: SettingsService,
                private commonservices: CommonService
        ) {
        // initially visible
        this.userBlockVisible  = true;
    }

    getUserBasicInfo() {
        return this.http.get(this.url.baseUrl + 'api/user/GetBasicInfo', this.commonservices.jwt())
            .flatMap((response: Response) => {
                return Promise.resolve(response);
            })
            .catch(this.commonservices.handleError);
    }

    getVisibility() {
        return this.userBlockVisible;
    }

    setVisibility(stat = true) {
        this.userBlockVisible = stat;
    }

    toggleVisibility() {
        this.userBlockVisible = !this.userBlockVisible;
    }

}
