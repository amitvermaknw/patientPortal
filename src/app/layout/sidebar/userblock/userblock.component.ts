import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { SettingsService } from '../../../core/settings/settings.service';
import { CommonService } from '../../../shared/common/common.service';
import { UserblockService } from './userblock.service';

@Component({
    selector: 'app-userblock',
    templateUrl: './userblock.component.html',
    styleUrls: ['./userblock.component.scss']
})
export class UserblockComponent implements OnInit {
    user: any;
    private responseObj;
    private userName: string;
    private profilePic: string;
    constructor(private userblockService: UserblockService,
                private http: Http,
                private commonservices: CommonService,
                private url: SettingsService,
                private router: Router,
                private route: ActivatedRoute,
            ) {this.getUserInfo();
    }

    ngOnInit() {
        this.commonservices.showLoader();
        
    }

    getUserInfo(){
        this.userblockService.getUserBasicInfo()
            .subscribe((data: Response) => {
                    if(data.status===200){
                        setTimeout(() => {
                            this.responseObj = data.json();
                            if(this.responseObj.StatusCode===200){
                                this.userName = this.responseObj.Data.FirstName+" "+this.responseObj.Data.MiddleName+" "+this.responseObj.Data.LastName;
                                this.profilePic = this.responseObj.Data.UserImage ? this.responseObj.Data.UserImage: 'assets/img/user/01.jpg';
                                this.commonservices.hideLoader();
                            }
                        
                        }, 100);
                    }else{
                         this.commonservices.MsgLookup('error', 'Error!!!', data.statusText);
                }    
            },
            (error) => {
                    let errMsg = JSON.parse(error);
                    if(errMsg.StatusCode===401){
                        this.commonservices.hideLoader(); //Loader
                        this.router.navigate([this.route.snapshot.queryParams['returnUrl'] || '/login']);
                        return false;
                    }
            });
    }

    userBlockIsVisible() {
        return this.userblockService.getVisibility();
    }

}
