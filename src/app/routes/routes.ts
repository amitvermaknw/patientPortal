import { LayoutComponent } from '../layout/layout.component';

import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { EmailvalidateComponent } from './pages/emailvalidate/emailvalidate.component';
import { AspatientComponent } from './pages/aspatient/aspatient.component'; 
import { RecoverComponent } from './pages/recover/recover.component';
import { LockComponent } from './pages/lock/lock.component';
import { MaintenanceComponent } from './pages/maintenance/maintenance.component';
import { Error404Component } from './pages/error404/error404.component';
import { Error500Component } from './pages/error500/error500.component';
import { AuthGuard } from '../shared/common/auth.guard';

export const routes = [

    { path: '', component: LoginComponent }, {
        path: '',
        component: LayoutComponent,
        children: [
            //{ path: '', redirectTo: 'login', pathMatch: 'full' },
            { path: 'home', loadChildren: './home/home.module#HomeModule' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'patient', loadChildren: './patient/patient.module#PatientModule', canActivate: [AuthGuard] },
            //{ path: 'addpatient', loadChildren: './patient/patient.module#PatientModule', canActivate: [AuthGuard] },
            { path: 'associatedHospitals', loadChildren: './hospitals/hospitals.module#HospitalsModule', canActivate: [AuthGuard] },
            { path: 'appointments', loadChildren: './appointments/appointments.module#AppointmentsModule' , canActivate: [AuthGuard]},
            { path: 'settings', loadChildren: './settings/settings.module#SettingsModule', canActivate: [AuthGuard]},
            
            { path: 'widgets', loadChildren: './widgets/widgets.module#WidgetsModule' },
            { path: 'elements', loadChildren: './elements/elements.module#ElementsModule' },
            { path: 'forms', loadChildren: './forms/forms.module#FormsModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'maps', loadChildren: './maps/maps.module#MapsModule' },
            { path: 'blog', loadChildren: './blog/blog.module#BlogModule' },
            { path: 'ecommerce', loadChildren: './ecommerce/ecommerce.module#EcommerceModule' },
            { path: 'extras', loadChildren: './extras/extras.module#ExtrasModule' }
        ]
    },

    // Not lazy-loaded routes
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent, canActivate: [AuthGuard] },
    { path: 'registration', component: RegistrationComponent },
    { path: 'activate', component: EmailvalidateComponent },
    { path: 'aspatient', component: AspatientComponent, canActivate: [AuthGuard]},
    { path: 'recover', component: RecoverComponent },
    { path: 'lock', component: LockComponent },
    { path: 'maintenance', component: MaintenanceComponent },
    { path: '404', component: Error404Component },
    { path: '500', component: Error500Component },

    // Not found
    //{ path: '**', redirectTo: 'home' }
    { path: '**', redirectTo: 'login' }

];
