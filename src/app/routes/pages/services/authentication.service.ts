import { Injectable } from '@angular/core';
import { Http, Jsonp, Headers, Response, RequestOptions, } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { SettingsService } from '../../../core/settings/settings.service';


@Injectable()
export class AuthenticationService {
    constructor(private http: Http, private url: SettingsService) {}

    login(username: string, password: string) {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var data = "grant_type=password&username=" + username + "&password=" + password;
        return this.http.post(this.url.baseUrl + 'token', data, { headers: headers })
            .map((response: Response) => { // login successful if there's a jwt token in the response
                let user = response.json();
                if (user && user.access_token) { // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }
            })
            .catch(this.handleError);
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            errMsg = body.error_description || JSON.stringify(body);
            //errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Promise.reject(errMsg);
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }

}
