import { Component, OnInit, ViewEncapsulation } from '@angular/core';
//import * as _ from 'underscore';
import { FormGroup, FormControl, FormBuilder, Validators, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { SettingsService } from '../../../core/settings/settings.service';
import { CommonService } from '../../../shared/common/common.service';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { RegistrationService, emailTaken } from '../registration/registration.service';

@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.scss'],
    styles: ['.ui-select-toggle1 {border-left:solid 5px #42A948 !important;}']
})

export class RegistrationComponent implements OnInit {
	valForm: FormGroup;
    constructor( 
        public settings: SettingsService,
        fb: FormBuilder,
        private commonservices: CommonService,
        public rs: RegistrationService 
    ) {
  	    this.valForm = fb.group({
            'FirstName': [null, Validators.required],
            'LastName': [null, Validators.required],
            'Email': [null, this.validEmail, emailTaken(this.rs)],
            'Phone': [null, Validators.compose([Validators.required, Validators.pattern('^[0-9]+$'), CustomValidators.rangeLength([10, 10])])],
            'Password': [null, Validators.compose([Validators.required, this.specialChar, this.passNumber, this.upperCase, this.passLength ])],
  			'ConfirmPassword': [null, Validators.required]
        }, {validator: commonservices.MatchPassword }); 
    }

    public validEmail(c: FormControl){
        let regex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        if(regex.test(c.value)){
            return null;
        }else {
            return { 'validEmail': true};
        }
    }
    
    public specialChar(c: FormControl){
        if(c.value){
            return /[&@#%*()!._-]/.test(c.value)==true? null : { 'specialChar': c.value };   
        }            
    }

    public passNumber(c: FormControl){
        return /[0-9]/.test(c.value)==true? null : { 'passNumber': c.value };
    }

    public upperCase(c: FormControl){
        return /[A-Z]/.test(c.value)==true? null : { 'upperCase': c.value };
    }

    public passLength(c: FormControl){
        if(c.value){
            return c.value.length >= 6? null : { 'passLength': c.value };
        }
    }

    submitForm($ev, formData: any) {
        $ev.preventDefault();
        this.commonservices.showLoader();
        this.rs.registration(formData).subscribe(
            comments => {
                if (comments.StatusCode===201) {
                    localStorage.setItem('userInfo', JSON.stringify(formData)); //Store userdata in localStorage
                    this.commonservices.hideLoader();
                    this.commonservices.MsgLookup('success', 'Success!!!', comments.Data);
                }
            },
            err => {
                let errMsg = JSON.parse(err);
                if(errMsg.StatusCode===400 || errMsg.StatusCode===401){
                    this.commonservices.hideLoader();
                    this.commonservices.MsgLookup('error', 'Fail!!!', errMsg.ErrorMessage);
                }
                else {
                    this.commonservices.hideLoader();
                    this.commonservices.MsgLookup('error', 'Fail!!!', "Something is wrong!!! Please contact Admin.");
                }
            }
        );
        
    }

	public ngOnInit(): any {}

}
