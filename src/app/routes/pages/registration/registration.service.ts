import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CommonService } from '../../../shared/common/common.service';
import { Http, Headers, Response } from '@angular/http';
import { SettingsService } from '../../../core/settings/settings.service';

@Injectable()
export class RegistrationService {

  	constructor(
  		private cs: CommonService, 
  		private http: Http, 
  		private url: SettingsService
  	) { }

  	public registration(formData): Observable < any > {
        return this.http.post(this.url.baseUrl + 'api/User/Create', formData, this.cs.jwt())
            .map((response: Response) => response.json())
        .catch(this.cs.handleError);
  	}

	public activeregistration(token): Observable < any > {
        return this.http.get(this.url.baseUrl + 'api/user/validate?token='+ token ,  this.cs.jwt())
            .map((response: Response) => response.json())
        .catch(this.cs.handleError);
  	}


  	public chekemail(formData): Observable < any > {
        return this.http.get(this.url.baseUrl + 'api/User/UserExist?email='+ formData , this.cs.jwt())
            .map((response: Response) => response.json())
        .catch(this.cs.handleError);
  	}
}

export function emailTaken(rs: RegistrationService) {
    return control => new Promise((resolve, reject) => {
		    rs.chekemail(control.value).subscribe(data => {
          if(data.Data) {
            resolve({ emailTaken : true})
          } else {
            resolve(null);
          }
        }, (err) => {
              if(err !== "404 - Not Found") {
                resolve({ emailTaken : true});
              } else {
                resolve(null);
              }
          });
    });
}
