import { Component, OnInit, ViewEncapsulation} from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { SettingsService } from '../../../core/settings/settings.service';
import { CommonService } from '../../../shared/common/common.service';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { AspatientService} from '../aspatient/aspatient.service';
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import 'rxjs/add/observable/of';

@Component({
  selector: 'app-aspatient',
  templateUrl: './aspatient.component.html',
  styleUrls: ['./aspatient.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AspatientComponent implements OnInit {

  	public valForm: FormGroup;
  	public countryName: Array<string> = [];
  	public results: any;
  	public countryId: any;
  	public stateId: any;
   	
    constructor( 
        public settings: SettingsService,
        fb: FormBuilder,
        private cs: CommonService,
        private patientService: AspatientService,
        private _sanitizer: DomSanitizer,
        private _router: Router
    ) {
  	    this.valForm = fb.group({
            'DateOfBirth': [null, Validators.compose([Validators.required, CustomValidators.date])],
            'Gender': [null, Validators.required],
            'Address1': [null, Validators.required],
            'Address2': '',
            'Address3': '',
            'Country': new FormControl('', Validators.required),
            'City': '',
            'State': '',
            'ZIP': [null, Validators.compose([Validators.required, Validators.pattern('^[0-9]+$'), CustomValidators.rangeLength([6, 7])])],
            'Relationship': new FormControl({value: 'Owner', disabled: true}, Validators.required)	
        }); 

    }

    ngOnInit() {
  	}

  	getCountry(formData: any) {
  		if(formData){
      		this.results=  this.patientService.getCountry(formData);
      		this.autocompleForCountry(this.results);
      		return this.results;
        }else{
        	return Observable.of([]);
        }
  	}

  	autocompleForCountry = (data: any) : SafeHtml => {
    let html = `<span>${data.CountryName}</span>`;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  	}

  	getState(formData: any) {
  		this.countryId = this.valForm.controls['Country'].value.CountryId;
  		if(formData){
      		this.results=  this.patientService.getState(formData, this.countryId);
      		this.autocompleForState(this.results);
      		return this.results;
        }else{
        	return Observable.of([]);
        }
  	}

  	autocompleForState = (data: any) : SafeHtml => {
    let html = `<span>${data.StateName}</span>`;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  	}

  	getCity(formData: any) {
  		this.stateId = this.valForm.controls['State'].value.StateId;
  		if(formData){
      		this.results=  this.patientService.getCity(formData, this.countryId, this.stateId);
      		this.autocompleForCity(this.results);
      		return this.results;
        }else{
        	return Observable.of([]);
        }
  	}

  	autocompleForCity = (data: any) : SafeHtml => {
    let html = `<span>${data.CityName}</span>`;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  	}

  	submitForm($ev, formData: any) {
        $ev.preventDefault();
        debugger;
        this.cs.showLoader();
        let userInfo = JSON.parse(localStorage.getItem('userInfo'));
        formData.FirstName = userInfo['FirstName'];
        formData.LastName = userInfo['LastName'];
        formData.Email =  userInfo['Email'];
        formData.Phone = userInfo['Phone'];
        formData.Country = this.countryId;
        formData.State = this.stateId;
        formData.City = this.valForm.controls['City'].value.CityId;

        this.patientService.addPatient(formData).subscribe(
            comments => {
                if (comments.StatusCode===201) {
                    this.cs.hideLoader();
                    this.cs.MsgLookup('success', 'Success!!!', comments.Data);
                }
            },
            err => {
            	let errMsg = JSON.parse(err);
                if(errMsg.StatusCode===400 || errMsg.StatusCode===401 ){
                    this.cs.hideLoader();
                    this.cs.MsgLookup('error', 'Fail!!!', errMsg.ErrorMessage);
                }
                else {
                    this.cs.hideLoader();
                    this.cs.MsgLookup('error', 'Fail!!!', "Something is wrong!!! Please contact Admin.");
                }
            }
        );
        
    }

    public cancle(){
    	this._router.navigate(['/patient']);
    }

}
