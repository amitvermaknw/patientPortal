import { TestBed, inject } from '@angular/core/testing';
import { AspatientService } from './aspatient.service';

describe('AspatientService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AspatientService]
    });
  });

  it('should ...', inject([AspatientService], (service: AspatientService) => {
    expect(service).toBeTruthy();
  }));
});
