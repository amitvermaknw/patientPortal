import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CommonService } from '../../../shared/common/common.service';
import { Http, Headers, Response } from '@angular/http';
import { SettingsService } from '../../../core/settings/settings.service';

@Injectable()
export class AspatientService {

  	constructor(
  		private cs: CommonService, 
  		private http: Http, 
  		private url: SettingsService
  		) { }

  	public addPatient(formData): Observable < any > {
        return this.http.post(this.url.baseUrl + 'api/patients/add', formData, this.cs.jwt())
            .map((response: Response) => response.json())
        .catch(this.cs.handleError);
  	}

  	public getCountry(formData): Observable < any > {
        return this.http.get(this.url.baseUrl + 'api/home/Countries?countryName='+ formData , this.cs.jwt())
            .map((response: Response) => {
            	let result  = response.json()
            	return result.Data; 
            })
        .catch(this.cs.handleError);
  	}

  	public getState(formData, countryId): Observable < any > {
        return this.http.get(this.url.baseUrl + 'api/home/States?countryid='+ countryId +'&statename='+ formData, this.cs.jwt())
            .map((response: Response) => {
            	let result  = response.json()
            	return result.Data; 
            })
        .catch(this.cs.handleError);
  	}

  	public getCity(formData, countryId, stateId): Observable < any > {
        return this.http.get(this.url.baseUrl + 'api/home/Cities?countryid='+ countryId +'&stateid='+ stateId +'&cityname='+ formData, this.cs.jwt())
            .map((response: Response) => {
            	let result  = response.json()
            	return result.Data; 
            })
        .catch(this.cs.handleError);
  	}
}
