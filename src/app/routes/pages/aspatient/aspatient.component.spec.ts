import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AspatientComponent } from './aspatient.component';

describe('AspatientComponent', () => {
  let component: AspatientComponent;
  let fixture: ComponentFixture<AspatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AspatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AspatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
