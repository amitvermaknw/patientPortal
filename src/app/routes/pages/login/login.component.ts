import { Component, OnInit, ElementRef, Renderer } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SettingsService } from '../../../core/settings/settings.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { AuthenticationService } from '../services/authentication.service';
import { CommonService } from '../../../shared/common/common.service';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    model: any = {};
    returnUrl: string;
    valForm: FormGroup;

    constructor(
        public settings: SettingsService,
        fb: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private commonservices: CommonService,
        public el: ElementRef,
        public renderer: Renderer) {

        this.valForm = fb.group({
            'email': [null, Validators.compose([Validators.required, CustomValidators.email])],
            'password': [null, Validators.required]
        });

    }

    submitForm($ev, value: any) {
        $ev.preventDefault();
        this.commonservices.showLoader(); //Loader
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(
                data => {
                    this.commonservices.MsgLookup('success', 'Success!!!', 'Login Successfully');
                    this.router.navigate([this.returnUrl]);
                    this.commonservices.hideLoader();
                },
                error => {
                    this.commonservices.MsgLookup('error', 'Fail!!!', error);
                    this.commonservices.hideLoader();
                });

    }

    ngOnInit() {
        // reset login status
        this.authenticationService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
    }

}
