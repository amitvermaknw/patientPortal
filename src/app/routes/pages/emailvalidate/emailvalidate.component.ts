import { Component, OnInit } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { SettingsService } from '../../../core/settings/settings.service';
import { CommonService } from '../../../shared/common/common.service';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { RegistrationService} from '../registration/registration.service';

@Component({
  selector: 'app-emailvalidate',
  templateUrl: './emailvalidate.component.html',
  styleUrls: ['./emailvalidate.component.scss']
})
export class EmailvalidateComponent implements OnInit {

	public alerts: Array<Object> = [];
  public dispay_options: any;
  	constructor(
  		public settings: SettingsService,
        private cs: CommonService,
        private http: Http,
        private rs: RegistrationService,
        private activatedRoute: ActivatedRoute,
        private router: Router
        ) { }

  	ngOnInit() {
      debugger;
  		  this.activatedRoute.params.subscribe((params: Params) => {
          this.activate(params['token']);
        });
    }

  	activate(token){
  		this.cs.showLoader();
        this.rs.activeregistration(token).subscribe(
            comments => {
                if (comments.StatusCode===201 || comments.StatusCode==200) {
                    this.cs.hideLoader();
                    this.alerts.push({ msg: 'Your Account has been Activated', type: 'success', closable: true });
                    this.dispay_options = true;
                }
            },
            err => {
                let errMsg = JSON.parse(err);
               	if(errMsg.StatusCode===400 || errMsg.StatusCode===500 ){
                    this.cs.hideLoader();
                    this.alerts.push({ msg: errMsg.ErrorMessage, type: 'danger', closable: true });
                }
                else {
                    this.cs.hideLoader();
                    this.alerts.push({ msg: 'Something is wrong. Please contact to admin!!!', type: 'error', closable: true });
                }
                this.dispay_options = false;
            }
        );
  	}

  	public closeAlert(i: number): void {
        this.alerts.splice(i, 1);
    }

    public asAccountOwner() {
    	this.router.navigate(['login']);
    }

    public asPatient() {
    	this.router.navigate(['/aspatient']);
    }
}
