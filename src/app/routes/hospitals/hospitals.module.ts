import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DataTableModule } from 'angular2-datatable';
import { Ng2TableModule } from 'ng2-table/ng2-table';
import { AgGridModule } from 'ag-grid-ng2/main';

import { SharedModule } from '../../shared/shared.module';
import { AssociatedhosComponent } from './associatedhos/associatedhos.component';

const routes: Routes = [
    { path: '', component: AssociatedhosComponent },
];

@NgModule({
    imports: [
        //CommonModule
        SharedModule,
        RouterModule.forChild(routes),
        DataTableModule,
        Ng2TableModule,
    ],
    declarations: [AssociatedhosComponent],
    exports: [
        RouterModule
    ]
})
export class HospitalsModule {}
