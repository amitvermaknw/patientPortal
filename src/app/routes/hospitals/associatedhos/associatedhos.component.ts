import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
import { TableData } from './ng2-table-data';
import { CommonService } from '../../../shared/common/common.service';
import { PatientlistService } from '../../patient/services/patientlist.service';


@Component({
    selector: 'app-associatedhos',
    templateUrl: './associatedhos.component.html',
    styleUrls: ['./associatedhos.component.scss']
})
export class AssociatedhosComponent implements OnInit {

    private results;
    showRecords: boolean = false; //hide patient record
    showHospitalList: boolean = true; //Show patient list
    private associatedHospitalList: Array < any > ;
    public hospitalList: Array < any > = [];
    public hospitalListArray: Array < any > = [];
    public listObj: any;

    //For patient record accordiance
    step1 = true;
    step2 = false;
    step3 = false;
    step4 = false;
    step5 = false;
    step6 = false;

    constructor(private http: Http, 
                private commonservices: CommonService,
                private patientlistservice: PatientlistService,
                private route: ActivatedRoute,
                private router: Router,
                ) {}

    getHospitalList(){
        this.patientlistservice.getPatientList().subscribe((data: Response) => {
            debugger;
                if(data.status===200){
                    setTimeout(() => {
                        this.results = data.json();
                        this.associatedHospitalList = this.results.Data;
                        this.length = this.associatedHospitalList.length;
                        this.associatedHospitalList.forEach((listObj) => {
                            listObj.Hospital.forEach((list: any) => {
                                this.hospitalListArray.push(list);
                            });
                        });
                        this.hospitalList = _.uniqBy(this.hospitalListArray, 'Code');
                        this.onChangeTable(this.config);
                        this.commonservices.hideLoader();
                    }, 100);
                }
                else{
                    this.commonservices.MsgLookup('error', 'Error!!!', data.statusText);
                }    

            },
            (error) => {
                    let errMsg = JSON.parse(error);
                    if(errMsg.StatusCode===401){
                        this.commonservices.hideLoader(); //Loader
                        this.commonservices.MsgLookup('error', 'Error!!!', errMsg.ErrorMessage);
                        this.router.navigate([this.route.snapshot.queryParams['returnUrl'] || '/login']);
                        return false;
                    }
            });
    }

    // angular2-datatable
    private sortByWordLength = (a: any) => {
        return a.name.length;
    }

    public removeItem(item: any) {
        this.results = _.filter(this.results, (elem) => elem != item);
        console.log('Remove: ', item.email);
    }

    public rows: Array < any > = [];
    public columns: Array < any > = [
        //{ title: 'Hospital Name', name: 'Name', filtering: { filterString: '', placeholder: 'Filter by name' } },
        { title: 'Hospital Name', name: 'Name'},
        { title: 'Address1', name: 'AddressLine1' },
        { title: 'Address2', name: 'AddressLine2' },
        { title: 'Address3', name: 'AddressLine3' },
        { title: 'City', className: 'text-warning', name: 'City' },
        { title: 'Zip', className: 'text-warning', name: 'Zip' },
        { title: 'Phone', className: 'text-warning', name: 'Phone' },
        { title: 'Website', className: 'text-warning', name: 'Website' },
        { title: '', name: 'actionSelect', sort: false },
    ];

    //Add button
    private selectBtn(actionSelect: string) {
        actionSelect = "<button class='mb-sm btn-sm btn btn-primary btn-outline' type='button'>Select</button>";
        return actionSelect;
    }

    private deletebtn(actionDelete: string) {
        actionDelete = "<button class='btn btn-info' type='button'><span class='glyphicon glyphicon-pencil'></span> Delete</button>";
        return actionDelete;
    }

    public page: number = 1;
    public itemsPerPage: number = 10;
    public maxSize: number = 5;
    public numPages: number = 1;
    public length: number = 0;


    public config: any = {
        paging: true,
        sorting: { columns: this.columns },
        filtering: { filterString: '' },
        className: ['table-striped', 'table-bordered', 'mb0', 'd-table-fixed', 'miyazaki'], // mb0=remove margin -/- .d-table-fixed=fix column width

    };


    public ngOnInit(): void {
        this.commonservices.mobileView();
        this.commonservices.showLoader();
        this.getHospitalList();
    }

    public changePage(page: any, data: Array < any > = this.associatedHospitalList): Array < any > {
        let start = (page.page - 1) * page.itemsPerPage;
        let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
        return data.slice(start, end);
    }

    public changeSort(data: any, config: any): any {
        if (!config.sorting) {
            return data;
        }

        let columns = this.config.sorting.columns || [];
        let columnName: string = void 0;
        let sort: string = void 0;

        for (let i = 0; i < columns.length; i++) {
            if (columns[i].sort !== '' && columns[i].sort !== false) {
                columnName = columns[i].name;
                sort = columns[i].sort;
            }
        }

        if (!columnName) {
            return data;
        }

        // simple sorting
        return data.sort((previous: any, current: any) => {
            if (previous[columnName] > current[columnName]) {
                return sort === 'desc' ? -1 : 1;
            } else if (previous[columnName] < current[columnName]) {
                return sort === 'asc' ? -1 : 1;
            }
            return 0;
        });
    }

    public changeFilter(data: any, config: any): any {

        let filteredData: Array < any > = data;
        this.columns.forEach((column: any) => {
            if (column.filtering) {
                filteredData = filteredData.filter((item: any) => {
                    let a = item[column.name];
                    if (item[column.name] != null && item[column.name] != '') {
                        return item[column.name].match(column.filtering.filterString);
                    } else {
                        return item[column.name];
                    }

                });
            }
        });

        if (!config.filtering) {
            return filteredData;
        }

        if (config.filtering.columnName) {
            return filteredData.filter((item: any) =>
                item[config.filtering.columnName].match(this.config.filtering.filterString));
        }

        let tempArray: Array < any > = [];
        filteredData.forEach((item: any) => {
            let flag = false;
            this.columns.forEach((column: any) => {
                if (item[column.name]) {
                    if (item[column.name].toString().match(this.config.filtering.filterString)) {
                        flag = true;
                    }
                }
            });
            if (flag) {
                tempArray.push(item);
            }
        });
        filteredData = tempArray;

        return filteredData;
    }

    public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
        if (config.filtering) {
            ( < any > Object).assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
            ( < any > Object).assign(this.config.sorting, config.sorting);
        }
        this.extendData(); //Add button 
        let filteredData = this.changeFilter(this.hospitalList, this.config);
        let sortedData = this.changeSort(filteredData, this.config);
        this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
        this.length = sortedData.length;

    }

    public onCellClick(data: any): any {
        this.showRecords = true; //Show Patient Record
        this.showHospitalList = false //Hide Patinet List
    }

    public onHospitalList(): any {
        this.showRecords = false; //Hide Patient Record
        this.showHospitalList = true //Show Patinet List
    }

    private extendData() {
        // For every resulttable entry
        for (let i = 0; i < this.hospitalList.length; i++) {
            // Add View Button
            this.hospitalList[i].actionSelect = this.selectBtn(this.hospitalList[i]);
        }
        return this.associatedHospitalList;
    }

}
