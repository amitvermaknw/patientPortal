import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssociatedhosComponent } from './associatedhos.component';

describe('AssociatedhosComponent', () => {
  let component: AssociatedhosComponent;
  let fixture: ComponentFixture<AssociatedhosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssociatedhosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssociatedhosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
