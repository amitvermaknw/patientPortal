import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DataTableModule } from 'angular2-datatable';
import { Ng2TableModule } from 'ng2-table/ng2-table';
import { AgGridModule } from 'ag-grid-ng2/main';

import { SharedModule } from '../../shared/shared.module';
import { PatientlistComponent } from './patientlist/patientlist.component';
import { PatientdetailsComponent } from './patientdetails/patientdetails.component';
import { AddpatientComponent } from './addpatient/addpatient.component';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';
import { AuthGuard } from '../../shared/common/auth.guard';
import { VisitdetailComponent } from './visitdetail/visitdetail.component';


const routes: Routes = [
    { path: '', component: PatientlistComponent },
    { path: 'addpatient', component: AddpatientComponent, canActivate: [AuthGuard] },
    { path: 'visit', component: VisitdetailComponent, canActivate: [AuthGuard] },
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        DataTableModule,
        Ng2TableModule,
        NguiAutoCompleteModule
        //AgGridModule.withComponents([AngulargridComponent])

    ],
    declarations: [
        PatientlistComponent,
        PatientdetailsComponent,
        AddpatientComponent,
        VisitdetailComponent,
        //PatientrecordsComponent
    ],
    exports: [
        RouterModule
    ]
})
export class PatientModule {}
