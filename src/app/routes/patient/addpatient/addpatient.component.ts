import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { SettingsService } from '../../../core/settings/settings.service';
import { CommonService } from '../../../shared/common/common.service';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { RegistrationService, emailTaken } from '../../pages/registration/registration.service';
import { AspatientService} from '../../pages/aspatient/aspatient.service';
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { ActivatedRoute, Router} from '@angular/router';
import 'rxjs/add/observable/of';

@Component({
  selector: 'app-addpatient',
  templateUrl: './addpatient.component.html',
  styleUrls: ['./addpatient.component.scss']
})
export class AddpatientComponent implements OnInit {

	valForm: FormGroup;
	public countryName: Array<string> = [];
  public results: any;
  public countryId: any;
  public stateId: any;
  public toaster: any;
  
    constructor( 
        public settings: SettingsService,
        fb: FormBuilder,
        private commonservices: CommonService,
        public rs: RegistrationService,
        private patientService: AspatientService,
        private _sanitizer: DomSanitizer,
         private route: ActivatedRoute,
        private router: Router

    ) {
  	    this.valForm = fb.group({
            'FirstName': [null, Validators.required],
            'MiddleName':'',
            'LastName': [null, Validators.required],
            //'Email': [null, this.validEmail, emailTaken(this.rs)],
            'Email': '',
            'Phone': [null, Validators.compose([Validators.required, Validators.pattern('^[0-9]+$'), CustomValidators.rangeLength([10, 10])])],
            'DateOfBirth': [null, Validators.compose([Validators.required, CustomValidators.date])],
            'Gender': [null, Validators.required],
            'Merital': [null, Validators.required],
            'Address1': [null, Validators.required],
            'Address2': '',
            'Address3': '',
            'Country': new FormControl('', Validators.required),
            'City': '',
            'State': '',
            'ZIP': [null, Validators.compose([Validators.required, Validators.pattern('^[0-9]+$'), CustomValidators.rangeLength([6, 7])])],
            'Relationship': [null, Validators.required]
        }); 

        //Configure the toaster
        this.toaster = this.commonservices.toasterconfig;
    }

    public validEmail(c: FormControl){
        let regex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        if(regex.test(c.value)){
            return null;
        }else {
            return { 'validEmail': true};
        }
    }
    
    public specialChar(c: FormControl){
        if(c.value){
            return /[&@#%*()!._-]/.test(c.value)==true? null : { 'specialChar': c.value };   
        }            
    }

    public passNumber(c: FormControl){
        return /[0-9]/.test(c.value)==true? null : { 'passNumber': c.value };
    }

    public upperCase(c: FormControl){
        return /[A-Z]/.test(c.value)==true? null : { 'upperCase': c.value };
    }

    public passLength(c: FormControl){
        if(c.value){
            return c.value.length >= 6? null : { 'passLength': c.value };
        }
    }

    getCountry(formData: any) {
  		if(formData){
      		this.results=  this.patientService.getCountry(formData);
      		this.autocompleForCountry(this.results);
      		return this.results;
        }else{
        	return Observable.of([]);
        }
  	}

  	autocompleForCountry = (data: any) : SafeHtml => {
    let html = `<span>${data.CountryName}</span>`;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  	}

  	getState(formData: any) {
  		this.countryId = this.valForm.controls['Country'].value.CountryId;
  		if(formData){
      		this.results=  this.patientService.getState(formData, this.countryId);
      		this.autocompleForState(this.results);
      		return this.results;
        }else{
        	return Observable.of([]);
        }
  	}

  	autocompleForState = (data: any) : SafeHtml => {
    let html = `<span>${data.StateName}</span>`;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  	}

  	getCity(formData: any) {
  		this.stateId = this.valForm.controls['State'].value.StateId;
  		if(formData){
      		this.results=  this.patientService.getCity(formData, this.countryId, this.stateId);
      		this.autocompleForCity(this.results);
      		return this.results;
        }else{
        	return Observable.of([]);
        }
  	}

  	autocompleForCity = (data: any) : SafeHtml => {
    let html = `<span>${data.CityName}</span>`;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  	}

    submitForm($ev, formData: any) {
      $ev.preventDefault();
      this.commonservices.showLoader();
      formData.City = formData.City.CityId;
        formData.State = formData.State.StateId;
        formData.Country = formData.Country.CountryId
        this.patientService.addPatient(formData).subscribe(
            comments => {
                if (comments.StatusCode===200) {
                    this.commonservices.hideLoader();
                    this.commonservices.MsgLookup('success', 'Success!!!', comments.Data);
                    setTimeout(() => { this.router.navigate([this.route.snapshot.queryParams['returnUrl'] || '/patient']);}, 5000)
                }
            },
            err => {
                let errMsg = JSON.parse(err);
                if(errMsg.StatusCode===400){
                    this.commonservices.hideLoader();
                    this.commonservices.MsgLookup('error', 'Fail!!!', errMsg.ErrorMessage);
                }
                else if(errMsg.StatusCode===401) {
                    this.commonservices.hideLoader();
                    this.commonservices.MsgLookup('error', 'Fail!!!', errMsg.ErrorMessage);
                    setTimeout(() => { this.router.navigate([this.route.snapshot.queryParams['returnUrl'] || '/login']);}, 5000)
                }
                  else{
                    this.commonservices.hideLoader();
                    this.commonservices.MsgLookup('error', 'Fail!!!', "Something is wrong!!! Please contact Admin.");
                }
            }
        );
        
    }

	public ngOnInit(): any {}

  public pathOwnerObj() {
    let OwnerInfo = JSON.parse(localStorage.getItem('OwnerInfo'));
    if(OwnerInfo.Status.GlobalStatusId===1){
       this.commonservices.MsgLookup('warning', 'Info!!!', "Owner already registered as a patient.");
       this.valForm.patchValue({
      'FirstName': OwnerInfo.FirstName,
      'MiddleName': OwnerInfo.MiddleName,
      'LastName': OwnerInfo.LastName,
      'Email': OwnerInfo.Email,
      'Phone':OwnerInfo.Phone, 
      });  
       //return;
    }
    else{
      this.valForm.patchValue({
      'FirstName': OwnerInfo.FirstName,
      'MiddleName': OwnerInfo.MiddleName,
      'LastName': OwnerInfo.LastName,
      'Email': OwnerInfo.Email,
      'Phone':OwnerInfo.Phone, 
      });  
    }
    
  }
}
