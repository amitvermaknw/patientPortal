import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl, FormBuilder, Validators, ValidatorFn } from '@angular/forms';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { ActivatedRoute, Routes, Router } from '@angular/router';
import { CommonService } from '../../../shared/common/common.service';

@Component({
  selector: 'app-visitdetail',
  templateUrl: './visitdetail.component.html',
  styleUrls: ['./visitdetail.component.scss']
})
export class VisitdetailComponent implements OnInit, AfterViewInit {

	@Input('patientVisitInfo') visitInfoObj: any;
	public viewPatientReportModel;
	public valForm: FormGroup;
	public visitId: any
  	
  	constructor(
        public fb: FormBuilder,
        private route: ActivatedRoute,
        private http: Http,
        private router: Router,
        private commonservices: CommonService, 
  		) 
  	{ 
  		this.route.queryParams.subscribe(params => {
  			this.visitId = JSON.parse(params['id']) ;
  		});
  	}

  	public setVisitDetails(visitId){
  		debugger;
  		let visitDetail = JSON.parse(sessionStorage.getItem('visitInfo'));
  		visitDetail = visitDetail.row;
  		if(visitDetail.PatientVisitId===visitId){
  			//let datePipe = new DatePipe("en-US");
        	//let dob = datePipe.transform(patient.DOB, 'yyyy-MM-dd');
        
    		this.valForm = this.fb.group({
            	'ReasonofVisit': [visitDetail.ReasonForVisit],
            	'MedicalHistory':[visitDetail.ReasonForVisit],
            	'BodyTemp': [visitDetail.Vitals[0].BodyTemperature],
            	'PulseRate': [visitDetail.Vitals[0].PulseRate],
            	'RespirationRate': [visitDetail.Vitals[0].RespiratoryRate],
            	'BloodPressure': [visitDetail.Vitals[0].BloodPressure],
            	'CurrentMedication': [visitDetail.CurrentMedications],
            	'MedicineName': [visitDetail.PrescribedMedication.MedicineName],
            	'Interval': [visitDetail.PrescribedMedication.MedicineInterval],
            	'SpecialInstruction': [visitDetail.PrescribedMedication.SpecialInstruction],
            	'PlanCare': [visitDetail.PlanOfCare],
            	'NextVisit': [visitDetail.DateOfNextVisit],
            	'LaboratoryTest': [visitDetail.LaboratoryTests],
            	'OtherTests': [visitDetail.RequestedLabTest[0].LabTest.LabTestDetails],
            	'Laboratory': [''],
        	});

  		} else {

  			this.commonservices.MsgLookup('error', 'Error!!!', 'Invalid visit id');
            this.router.navigate([this.route.snapshot.queryParams['returnUrl'] || '/patient']);
            return false;
  		}
    	 
    }

  	ngOnInit() {
 		this.setVisitDetails(this.visitId);
  	}

  	@ViewChild('modal') modal: ElementRef
    ngAfterViewInit() {
        this.modal.nativeElement.focus();
    }

    loadModel(){
    	debugger;
    	console.log('test2')
    }

}
