import { Component, OnInit, Input  } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import * as _ from 'lodash';
import { CommonService } from '../../../shared/common/common.service';
import { PatientlistService } from '../services/patientlist.service';

declare let $: any;

@Component({
    selector: 'app-patientdetails',
    templateUrl: './patientdetails.component.html',
    styleUrls: ['./patientdetails.component.scss']
})
export class PatientdetailsComponent implements OnInit  {

    @Input('patientReportDetail') patientReportObj: any;

    public singleData;
    public patientVisitList;
    public patientReportsDetail;
    public showReports: boolean;
    public showAlerts: boolean;
    public page: number = 1;
    public itemsPerPage: number = 10;
    public maxSize: number = 5;
    public numPages: number = 1;
    public length: number = 0;
    public IsVisitInfo:boolean;
    public visitInfo;
    public visitVisible: boolean;

    constructor(
        private http: Http,
        private commonservices: CommonService,
        private route: ActivatedRoute,
        private router: Router,
        private patientlistServices: PatientlistService
    ) {}


    public ngOnInit(): void {
        this.commonservices.showLoader(); 
        this.getPatientDetails();

    }

    public rows: Array < any > = [];
    public columns: Array < any > = [
        { title: 'Date', name: 'DateOfVisit', },
        { title: 'Hospital', className: 'text-warning', name: 'Hospital.Name' },
        { title: 'Reason for visit', className: 'text-warning', name: 'ReasonForVisit' },
        { title: '', name: 'viewReport', sort: false, value: 'PatientVisitId' },
    ];

    public config: any = {
        paging: true,
        sorting: { columns: this.columns },
        filtering: { filterString: '' },
        className: ['table-striped', 'table-bordered', 'mb0', 'd-table-fixed'], // mb0=remove margin -/- .d-table-fixed=fix column width

    };

    private getPatientDetails() {
        this.patientlistServices.getPatientDetail(this.patientReportObj.PatientId)
            .subscribe((data: Response) => {
                if(data.status===200){
                    setTimeout(() => {
                        this.patientReportsDetail = data.json();
                        if (this.patientReportsDetail.Data.length) {
                            this.patientVisitList = this.patientReportsDetail;
                            this.onChangeTable(this.config);
                            this.showReports = true;
                            this.showAlerts = false;
                        } else {
                            this.showReports = false;
                            this.showAlerts = true;
                            this.commonservices.addAlert('Data is not available', 'danger');
                        }
                        this.commonservices.hideLoader();
                    }, 100);
                }    
            },
                (error) => {
                    let errMsg = JSON.parse(error);
                    if(errMsg.StatusCode===401){
                        this.commonservices.hideLoader(); //Loader
                        this.commonservices.MsgLookup('error', 'Error!!!', errMsg.ErrorMessage);
                        this.router.navigate([this.route.snapshot.queryParams['returnUrl'] || '/login']);
                        return false;
                    }
                });
    }

    private sortByWordLength = (a: any) => {
        return a.name.length;
    }

    public changePage(page: any, data: Array < any > = this.patientReportsDetail): Array < any > {
        let start = (page.page - 1) * page.itemsPerPage;
        let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
        return data.slice(start, end);
    }

    public changeSort(data: any, config: any): any {
        if (!config.sorting) {
            return data;
        }

        let columns = this.config.sorting.columns || [];
        let columnName: string = void 0;
        let sort: string = void 0;

        for (let i = 0; i < columns.length; i++) {
            if (columns[i].sort !== '' && columns[i].sort !== false) {
                columnName = columns[i].name;
                sort = columns[i].sort;
            }
        }

        if (!columnName) {
            return data;
        }

        // simple sorting
        return data.sort((previous: any, current: any) => {
            if (previous[columnName] > current[columnName]) {
                return sort === 'desc' ? -1 : 1;
            } else if (previous[columnName] < current[columnName]) {
                return sort === 'asc' ? -1 : 1;
            }
            return 0;
        });
    }

    public changeFilter(data: any, config: any): any {

        let filteredData: Array < any > = data;
        this.columns.forEach((column: any) => {
            if (column.filtering) {
                filteredData = filteredData.filter((item: any) => {
                    return item[column.name].match(column.filtering.filterString);
                });
            }
        });

        if (!config.filtering) {
            return filteredData;
        }

        if (config.filtering.columnName) {
            return filteredData.filter((item: any) =>
                item[config.filtering.columnName].match(this.config.filtering.filterString));
        }

        let tempArray: Array < any > = [];
        filteredData.forEach((item: any) => {
            let flag = false;
            this.columns.forEach((column: any) => {
                if (item[column.name]) {
                    if (item[column.name].toString().match(this.config.filtering.filterString)) {
                        flag = true;
                    }
                }
            });
            if (flag) {
                tempArray.push(item);
            }
        });
        filteredData = tempArray;

        return filteredData;
    }

    public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
        if (config.filtering) {
            ( < any > Object).assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
            ( < any > Object).assign(this.config.sorting, config.sorting);
        }
        this.extendData(); //Add button 
        let filteredData = this.changeFilter(this.patientVisitList.Data, this.config);
        let sortedData = this.changeSort(filteredData, this.config);
        this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
        this.length = sortedData.length;

    }

    //Add button
    private viewReportBtn(viewReport: string) {
        viewReport = "<button class='mb-sm btn-sm btn btn-primary btn-outline' type='button'>View Report</button>";
        return viewReport;
    }

    private extendData() {
        for (let i = 0; i < this.patientVisitList.Data.length; i++) {
            // Add View Button
            this.patientVisitList.Data[i].viewReport = this.viewReportBtn(this.patientVisitList.Data[i]);
        }
        return this.patientVisitList;
    }

    private visitDetail(visitInfo){
        debugger;
        sessionStorage.setItem('visitInfo', JSON.stringify(visitInfo));
        let navigationExtras: NavigationExtras ={
            queryParams: {
                id: visitInfo.row.PatientVisitId
            }
        }
        this.router.navigate([this.route.snapshot.queryParams['returnUrl'] || '/patient/visit'], navigationExtras);
    }
}
