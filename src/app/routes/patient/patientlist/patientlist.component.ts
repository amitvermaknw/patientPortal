import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import * as _ from 'lodash';
import { CommonService } from '../../../shared/common/common.service';
import { SettingsService } from '../../../core/settings/settings.service';
import { PatientlistService } from '../services/patientlist.service';
import { PatientdetailsComponent } from '../patientdetails/patientdetails.component'


@Component({
    selector: 'app-patientlist',
    templateUrl: './patientlist.component.html',
    styleUrls: ['./patientlist.component.scss'],
    //directives: [PatientdetailsComponent],
})
export class PatientlistComponent implements OnInit {

    private responseObj;
    private associatedPatientList: Array < any > ;
    public patientBasicInfo;
    public showRecords: boolean = false; 
    public showPatientList: boolean = true; 
    public IsVisitInfo: boolean;
    public patientList: Array <any>=[];
    public patientData: any;
    public toaster: any = {};

    //For patient record accordiance
    step1 = true;
    step2 = false;
    step3 = false;
    step4 = false;
    step5 = false;
    step6 = false;

    //Table Configuration
    public page: number = 1;
    public itemsPerPage: number = 10;
    public maxSize: number = 5;
    public numPages: number = 1;
    public length: number = 0;

    constructor(
        private http: Http,
        private commonservices: CommonService,
        private url: SettingsService,
        private route: ActivatedRoute,
        private router: Router,
        private patientlistServices: PatientlistService
    ) { this.getPatientList(); }


    public rows: Array < any > = [];
    public columns: Array < any > = [
        //{ title: 'First Name', name: 'FirstName', filtering: { filterString: '', placeholder: 'Filter by name' } },
        { title: 'First Name', name: 'FirstName'},
        { title: 'Last Name', name: 'LastName'},
        { title: 'DOB', className: 'text-warning', name: 'DOB' },
        { title: 'Email', className: 'text-warning', name: 'Emailid' },
        { title: 'Phone', className: 'text-warning', name: 'Phone' },
        { title: 'City', className: 'text-warning', name: 'City' },
        { title: '', name: 'actionSelect', sort: false },
    ];

    public config: any = {
        paging: true,
        sorting: { columns: this.columns },
        filtering: { filterString: '' },
        className: ['table-striped', 'table-bordered', 'mb0', 'd-table-fixed'], // mb0=remove margin -/- .d-table-fixed=fix column width

    };

    private getPatientList() {
        this.patientlistServices.getPatientList()
            .subscribe((data: Response) => {
                    if(data.status===200){
                        setTimeout(() => {
                            this.responseObj = data.json();
                            if(this.responseObj.StatusCode===200){
                                this.associatedPatientList = this.responseObj;
                                this.length = this.responseObj.Data.length;
                                this.prepPatientList(this.responseObj.Data);
                                this.patientList = this.responseObj.Data;
                                this.onChangeTable(this.config);
                                this.storeAccountOwnerInfo(this.responseObj);
                                this.commonservices.hideLoader();
                            }
                        
                        }, 100);
                    }else{
                         this.commonservices.MsgLookup('error', 'Error!!!', data.statusText);
                    }    
            },
            (error) => {
                    let errMsg = JSON.parse(error);
                    if(errMsg.StatusCode===401){
                        this.commonservices.hideLoader();
                        this.commonservices.MsgLookup('error', 'Error!!!', errMsg.ErrorMessage);
                        this.router.navigate([this.route.snapshot.queryParams['returnUrl'] || '/login']);
                        return false;
                    }
                    if(errMsg.StatusCode===404){
                        this.commonservices.hideLoader();
                        this.commonservices.MsgLookup('error', 'Error!!!', errMsg.ErrorMessage);
                        return false;
                    }
            });
    }

    public prepPatientList(patientObj: any): Array <any>{
        return patientObj.forEach((row: any)=>{
            row.City = row.City
        });
    }
    private sortByWordLength = (a: any) => {
        return a.name.length;
    }

    public removeItem(item: any) {
        this.responseObj = _.filter(this.responseObj, (elem) => elem != item);
        console.log('Remove: ', item.email);
    }

    //Add button
    private selectBtn(actionSelect: string) {
        actionSelect = "<button class='mb-sm btn-sm btn btn-primary btn-outline' type='button'>Select</button>";
        return actionSelect;
    }

    private deletebtn(actionDelete: string) {
        actionDelete = "<button class='btn btn-info' type='button'><span class='glyphicon glyphicon-pencil'></span> Delete</button>";
        return actionDelete;
    }

    public ngOnInit(): void {
        this.commonservices.showLoader();
        this.commonservices.mobileView();
    }

    public changePage(page: any, data: Array < any > = this.associatedPatientList): Array < any > {
        let start = (page.page - 1) * page.itemsPerPage;
        let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
        return data.slice(start, end);
    }

    public changeSort(data: any, config: any): any {
        if (!config.sorting) {
            return data;
        }

        let columns = this.config.sorting.columns || [];
        let columnName: string = void 0;
        let sort: string = void 0;

        for (let i = 0; i < columns.length; i++) {
            if (columns[i].sort !== '' && columns[i].sort !== false) {
                columnName = columns[i].name;
                sort = columns[i].sort;
            }
        }

        if (!columnName) {
            return data;
        }

        // simple sorting
        return data.sort((previous: any, current: any) => {
            if (previous[columnName] > current[columnName]) {
                return sort === 'desc' ? -1 : 1;
            } else if (previous[columnName] < current[columnName]) {
                return sort === 'asc' ? -1 : 1;
            }
            return 0;
        });
    }

    public changeFilter(data: any, config: any): any {
        let filteredData: Array < any > = data;
        this.columns.forEach((column: any) => {
            if (column.filtering) {
                filteredData = filteredData.filter((item: any) => {
                    return item[column.name].match(column.filtering.filterString);
                });
            }
        });

        if (!config.filtering) {
            return filteredData;
        }

        if (config.filtering.columnName) {
            return filteredData.filter((item: any) =>
                item[config.filtering.columnName].match(this.config.filtering.filterString));
        }

        let tempArray: Array < any > = [];
        filteredData.forEach((item: any) => {
            let flag = false;
            this.columns.forEach((column: any) => {
                if (item[column.name]) {
                    if (item[column.name].toString().match(this.config.filtering.filterString)) {
                        flag = true;
                    }
                }
            });
            if (flag) {
                tempArray.push(item);
            }
        });
        filteredData = tempArray;

        return filteredData;
    }

    public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
        if (config.filtering) {
            ( < any > Object).assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
            ( < any > Object).assign(this.config.sorting, config.sorting);
        }
        this.extendData(); //Add button 
        let filteredData = this.changeFilter(this.patientList, this.config);
        let sortedData = this.changeSort(filteredData, this.config);
        this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
        this.length = sortedData.length;

    }

    private extendData() {
        for (let i = 0; i < this.length; i++) {
            // Add View Button
            this.patientList[i].actionSelect = this.selectBtn(this.patientList[i]);
        }
        return this.patientList;
    }

    public getPatientDetail(data: any): any {
        this.commonservices.showLoader(); //Add Loader
        this.patientBasicInfo = data.row; //get patient basic information
        this.showRecords = true; //Hide Patient Record
        this.showPatientList = false //Show Patinet List
        this.IsVisitInfo = false;
        this.patientData = data.row;
        this.commonservices.hideLoader(); // Remove Loader
    }

    public onPatientList(): any {
        this.showRecords = false; //Hide Patient Record
        this.showPatientList = true //Show Patinet List
    }

    public addPatient(): any{
        this.router.navigate([this.route.snapshot.queryParams['returnUrl'] || '/patient/addpatient']);
        //this.router.navigate(['/patient/addpatient']);
    }

    public storeAccountOwnerInfo(data: any): any{
        let ownerObj ={
            'FirstName': data.Data['FirstName'],
            'MiddleName':data.Data['MiddleName'],
            'LastName':data.Data['LastName'],
            'Email':data.Data['LoginEmailID'],
            'Phone':data.Data['Phone'],
            'Status': data.Data['Status'] 
        }

        localStorage.setItem('OwnerInfo', JSON.stringify(ownerObj));
    }

}
