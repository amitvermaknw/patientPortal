import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { SettingsService } from '../../../core/settings/settings.service';
import { CommonService } from '../../../shared/common/common.service';

@Injectable()
export class PatientlistService {

    constructor(
        private http: Http,
        private url: SettingsService,
        private commonservices: CommonService) {}

    getPatientList() {
        //return this.http.get(this.url.baseUrl + 'api/user/getuserinfo', this.commonservices.jwt())
        return this.http.get(this.url.baseUrl + 'api/patients/getall', this.commonservices.jwt())
            .flatMap((response: Response) => {
                return Promise.resolve(response);
            })
            .catch(this.commonservices.handleError);
    }

    getPatientDetail(patientId: any) {
        return this.http.get(this.url.baseUrl + 'api/patients/' + patientId + '/visits/', this.commonservices.jwt())
            .flatMap((response: Response) => {
                return Promise.resolve(response);
            })
            .catch(this.commonservices.handleError);
    }
}
