import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl, FormBuilder, Validators, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { SettingsService } from '../../../core/settings/settings.service';
import { CommonService } from '../../../shared/common/common.service';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { RegistrationService, emailTaken } from '../../pages/registration/registration.service';
import { AspatientService} from '../../pages/aspatient/aspatient.service';
import { ProfileService} from '../services/profile.service';
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { ActivatedRoute, Router} from '@angular/router';
import 'rxjs/add/observable/of';

@Component({
  selector: 'app-updatepatients',
  templateUrl: './updatepatients.component.html',
  styleUrls: ['./updatepatients.component.scss']
})
export class UpdatepatientsComponent implements OnInit {

	@Input('patientProfileDetail') patientDetail: any;

  	public valForm: FormGroup;
  	public countryName: Array<string> = [];
  	public results: any;
  	public countryId: any;
  	public stateId: any;
  	public toaster: any;
  	public gender: any[] = [
      			{ "id": 1, "name": "Male" },
      			{ "id": 2, "name": "Female" },
    		];
  
    constructor( 
        public settings: SettingsService,
        public fb: FormBuilder,
        private commonservices: CommonService,
        public rs: RegistrationService,
        private patientService: AspatientService,
        private _sanitizer: DomSanitizer,
        private route: ActivatedRoute,
        private router: Router,
        private profileServices: ProfileService,

    ) {

    }

    public setPatientProfile(patient){
    	let datePipe = new DatePipe("en-US");
        let dob = datePipe.transform(patient.DOB, 'yyyy-MM-dd');

    	this.valForm = this.fb.group({
            'FirstName': [{value: patient.FirstName, disabled: true}, Validators.required],
            'MiddleName':[{value: patient.MIddleName, disabled: true}],
            'LastName': [{value: patient.LastName, disabled: true}, Validators.required],
            'Email': patient.Emailid,
            'Phone': [patient.Phone, Validators.compose([Validators.required, Validators.pattern('^[0-9]+$'), CustomValidators.rangeLength([10, 10])])],
            'DateOfBirth': [{value: dob, disabled: true}, Validators.compose([Validators.required, CustomValidators.date])],
            'Gender': [patient.Gender, Validators.required],
            'Address1': [patient.AddressLine1, Validators.required],
            'Address2': patient.AddressLine2,
            'Address3': patient.AddressLine3,
            'Country': new FormControl(patient.Country, Validators.required),
            'City': patient.City,
            'State': patient.State,
            'ZIP': [patient.Zip, Validators.compose([Validators.required, Validators.pattern('^[0-9]+$'), CustomValidators.rangeLength([6, 7])])],
        }); 

        this.toaster = this.commonservices.toasterconfig;
    }

    public validEmail(c: FormControl){
        let regex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        if(regex.test(c.value)){
            return null;
        }else {
            return { 'validEmail': true};
        }
    }
    
    public specialChar(c: FormControl){
        if(c.value){
            return /[&@#%*()!._-]/.test(c.value)==true? null : { 'specialChar': c.value };   
        }            
    }

    public passNumber(c: FormControl){
        return /[0-9]/.test(c.value)==true? null : { 'passNumber': c.value };
    }

    public upperCase(c: FormControl){
        return /[A-Z]/.test(c.value)==true? null : { 'upperCase': c.value };
    }

    public passLength(c: FormControl){
        if(c.value){
            return c.value.length >= 6? null : { 'passLength': c.value };
        }
    }

    getCountry(formData: any) {
  		if(formData){
      		this.results=  this.patientService.getCountry(formData);
      		this.autocompleForCountry(this.results);
      		return this.results;
        }else{
        	return Observable.of([]);
        }
  	}

  	autocompleForCountry = (data: any) : SafeHtml => {
    let html = `<span>${data.CountryName}</span>`;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  	}

  	getState(formData: any) {
  		this.countryId = this.valForm.controls['Country'].value.CountryId;
  		if(formData){
      		this.results=  this.patientService.getState(formData, this.countryId);
      		this.autocompleForState(this.results);
      		return this.results;
        }else{
        	return Observable.of([]);
        }
  	}

  	autocompleForState = (data: any) : SafeHtml => {
    let html = `<span>${data.StateName}</span>`;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  	}

  	getCity(formData: any) {
  		this.stateId = this.valForm.controls['State'].value.StateId;
  		if(formData){
      		this.results=  this.patientService.getCity(formData, this.countryId, this.stateId);
      		this.autocompleForCity(this.results);
      		return this.results;
        }else{
        	return Observable.of([]);
        }
  	}

  	autocompleForCity = (data: any) : SafeHtml => {
    let html = `<span>${data.CityName}</span>`;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  	}

    submitForm($ev, formData: any) {
      $ev.preventDefault();
      this.commonservices.showLoader();
      formData.City = formData.City.CityId;
        formData.State = formData.State.StateId;
        formData.Country = formData.Country.CountryId
        this.profileServices.updatePatient(formData).subscribe(
            comments => {
                if (comments.StatusCode===200) {
                    this.commonservices.hideLoader();
                    this.commonservices.MsgLookup('success', 'Success!!!', comments.Data);
                    setTimeout(() => { this.router.navigate([this.route.snapshot.queryParams['returnUrl'] || '/patient']);}, 5000)
                }
            },
            err => {
                let errMsg = JSON.parse(err);
                if(errMsg.StatusCode===400){
                    this.commonservices.hideLoader();
                    this.commonservices.MsgLookup('error', 'Fail!!!', errMsg.ErrorMessage);
                }
                else if(errMsg.StatusCode===401) {
                    this.commonservices.hideLoader();
                    this.commonservices.MsgLookup('error', 'Fail!!!', errMsg.ErrorMessage);
                    setTimeout(() => { this.router.navigate([this.route.snapshot.queryParams['returnUrl'] || '/login']);}, 5000)
                }
                  else{
                    this.commonservices.hideLoader();
                    this.commonservices.MsgLookup('error', 'Fail!!!', "Something is wrong!!! Please contact Admin.");
                }
            }
        );
        
    }

  ngOnInit() {
  	this.setPatientProfile(this.patientDetail);
  	console.log(this.patientDetail);
  }

}
