import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { SettingsService } from '../../../core/settings/settings.service';
import { CommonService } from '../../../shared/common/common.service';


@Injectable()
export class ProfileService {

  	constructor(private http: Http,
        private url: SettingsService,
        private commonservices: CommonService) { }


  	public updatePatient(formData): Observable < any > {
        return this.http.post(this.url.baseUrl + 'api/patients/update', formData, this.commonservices.jwt())
            .map((response: Response) => response.json())
        .catch(this.commonservices.handleError);
  	}

}
