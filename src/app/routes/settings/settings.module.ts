import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TreeModule } from 'angular2-tree-component';
import { DndModule } from 'ng2-dnd';
import { Ng2TableModule } from 'ng2-table/ng2-table';

import { SharedModule } from '../../shared/shared.module';
import { ProfileComponent } from './profile/profile.component';
import { UpdatepatientsComponent } from './update/updatepatients.component';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';

const route: Routes = [
    { path: 'profile', component: ProfileComponent },
];

@NgModule({
    imports: [
    	SharedModule,
        CommonModule,
        RouterModule.forChild(route),
        TreeModule,
        Ng2TableModule,
        DndModule.forRoot(),
        NguiAutoCompleteModule,
    ],
    declarations: [
    	ProfileComponent,
    	UpdatepatientsComponent
    ],
    exports: [
    	RouterModule
    ]
})
export class SettingsModule {
}
