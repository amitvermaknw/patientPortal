import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SelectModule } from 'ng2-select';
import { SharedModule } from '../../shared/shared.module';
import { AppointmentsComponent } from './appointments/appointments.component';

const routes: Routes = [
    { path: '', component: AppointmentsComponent },
];

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        RouterModule.forChild(routes),
        SelectModule
    ],
    declarations: [AppointmentsComponent],
    exports: [
        RouterModule
    ]
})
export class AppointmentsModule {}
